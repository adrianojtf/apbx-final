/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.user;

import br.com.usj.user.exceptions.NonexistentEntityException;
import br.com.usj.user.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;

/**
 *
 * @author adriano
 */
public class UserJpaController implements Serializable {
    public static UserJpaController users = new UserJpaController();
    
    
    
    EntityManager em = Persistence.
            createEntityManagerFactory("apbxPU").
            createEntityManager();
    
    public User validarUser(String usur, String senha){
       
        TypedQuery<User> query = this.em.createQuery("select u from User u where u.user = :login and u.password = :senha", User.class);
        query.setParameter("login", usur);
        query.setParameter("senha", senha);

        try{
            return query.getSingleResult();
        }catch(NoResultException e){
            e.printStackTrace();
            return null;
        }
        
        
    }
    
    public List<User> list(){
        String query = "SELECT s FROM User s";
        
        return em.createQuery(query).getResultList();
    }
    
    public void add(User s){
        if(!em.getTransaction().isActive()){
            em.getTransaction().begin();
            em.persist(s);
            em.getTransaction().commit();
            
        }


    }

    public void edit(User s){
        long id = s.getId();
        em.getTransaction().begin();
        User sub= em.find(User.class, id);
        em.merge(s);
        sub.setUser(s.getUser());
        sub.setPassword(s.getPassword());
     
        em.getTransaction().commit();
        
    }
    
    public void remove(User s){
        em.getTransaction().begin();
        em.remove(s);
        em.getTransaction().commit();
    }
    
    public User findById(long id){
      
        return em.find(User.class, id);
    }
    
}
