/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.user;

import static br.com.usj.user.UserJpaController.users;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author adriano
 */
@ManagedBean
@ViewScoped
public class BeanUser implements Serializable {

    /**
     * Creates a new instance of BeanUser
     */
    
    private User user;
    private String username;
    private String password;
    private User userSelect = new User();
    private User userPersist = new User();
    private List<User> userList;
    
    public BeanUser() {
        user = new User();
    }

    public User getUserSelect() {
        return userSelect;
    }

    public void setUserSelect(User userSelect) {
        this.userSelect = userSelect;
    }

    public User getUserPersist() {
        return userPersist;
    }

    public void setUserPersist(User userPersist) {
        this.userPersist = userPersist;
    }

    public List<User> getUserList() {
        return users.list();
    }

    public void setUserList(List<User> userList) {
        this.userList = userList;
    }
    
    
    
    
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public String validarUser(){
        int tamanhoU = this.user.getUser().length();
        int tamanhoP = this.user.getPassword().length();
        if(tamanhoU!=0 || tamanhoP!=0){
            User valUser = users.validarUser(this.user.getUser(),this.user.getPassword());
            
            if(valUser!=null){
                String usernameV = valUser.getUser();
                String passwordV = valUser.getPassword();
                
                return "index.xhtml?face-redirect=true"; 
            }
            else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Usuário e/ou senha invalido(s)"));
                return null;
        }
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Usuário e/ou senha vazio(s)"));
            return null;
        }
        
        /**
        String usernameV = "teste";
        String passwordV = "teste";
        */        
    } 
    
    public String registraSaida() {

		//REMOVER USUARIO DA SESSION
		
		
		return "/login?faces-redirect=true";
	}
    
    
   public void findById(long id){
        System.out.println(id);
        userSelect = users.findById(id);
        
    }
    
    public void edit(){
        int tamanhoA = this.userSelect.getUser().length();
        int tamanhoP = this.userSelect.getPassword().length();
        if(tamanhoA!=0 && tamanhoP!=0){
            users.edit(userSelect);
            FacesContext context = FacesContext.getCurrentInstance(); 
            context.addMessage(null, new FacesMessage("Sucesso!!",  "Ramal Alterado!"));
            context.addMessage(null, new FacesMessage("Cuidado!!!", "Certifique-se de que escolheu uma senha segura!"));
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Ramal ou senha nulo(s)!!"));
        }
    }
    
    public void remove(){
        users.remove(userSelect);
        FacesContext context = FacesContext.getCurrentInstance(); 
        context.addMessage(null, new FacesMessage("Sucesso",  "Ramal excluido com sucesso!"));
    }
    
    public void add(){
        
        
        int tamanhoA = this.userPersist.getUser().length();
        int tamanhoP = this.userPersist.getPassword().length();
        if(tamanhoA!=0 || tamanhoP!=0){
            users.add(userPersist);
            FacesContext context = FacesContext.getCurrentInstance(); 
            context.addMessage(null, new FacesMessage("Sucesso!!",  "Ramal criado!"));
            context.addMessage(null, new FacesMessage("Cuidado!!!", "Certifique-se de que escolheu uma senha segura!"));
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Ramal ou senha nulo(s)!!"));
        } 
    }
    
}
    
