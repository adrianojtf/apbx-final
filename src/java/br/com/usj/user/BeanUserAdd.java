/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.user;

import static br.com.usj.user.UserJpaController.users;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author adriano
 */
@ManagedBean
@RequestScoped
public class BeanUserAdd implements Serializable {

    /**
     * Creates a new instance of BeanUser
     */
    
    
    private User userPersist = new User();
    
    
    

    public User getUserPersist() {
        return userPersist;
    }

    public void setUserPersist(User userPersist) {
        this.userPersist = userPersist;
    }

   
    
    public void add(){
        
        
        int tamanhoA = this.userPersist.getUser().length();
        int tamanhoP = this.userPersist.getPassword().length();
        if(tamanhoA!=0 || tamanhoP!=0){
            users.add(userPersist);
            FacesContext context = FacesContext.getCurrentInstance(); 
            context.addMessage(null, new FacesMessage("Sucesso!!",  "Ramal criado!"));
            context.addMessage(null, new FacesMessage("Cuidado!!!", "Certifique-se de que escolheu uma senha segura!"));
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Ramal ou senha nulo(s)!!"));
        } 
    }
    
}
    
