/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.user;

import br.com.usj.apbx.util.jsf.SessionUtil;
import static br.com.usj.user.UserJpaController.users;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author adriano
 */
@ManagedBean
@SessionScoped
public class BeanUserLogin implements Serializable {

    /**
     * Creates a new instance of BeanUser
     */
    
    private User user;
    private String username;
    private String password;
    
    
    public BeanUserLogin() {
        user = new User();
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
    
    public String validarUser(){
        int tamanhoU = this.user.getUser().length();
        int tamanhoP = this.user.getPassword().length();
        if(tamanhoU!=0 || tamanhoP!=0){
            User valUser = users.validarUser(this.user.getUser(),this.user.getPassword());
            
            if(valUser!=null){
                String usernameV = valUser.getUser();
                String passwordV = valUser.getPassword();
                
                Object b = new Object();
			
			SessionUtil.setParam("USUARIOLogado", b);
                
                return "index.xhtml?face-redirect=true"; 
            }
            else{
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Usuário e/ou senha invalido(s)"));
                return null;
        }
        }
        else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Usuário e/ou senha vazio(s)"));
            return null;
        }
        
        /**
        String usernameV = "teste";
        String passwordV = "teste";
        */        
    } 
    
    public String registraSaida() {

		//REMOVER USUARIO DA SESSION
		
		
		return "/login?faces-redirect=true";
	}
    
    
   
    
}
    
