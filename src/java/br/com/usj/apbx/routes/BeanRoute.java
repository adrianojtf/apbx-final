/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.apbx.routes;


import static br.com.usj.apbx.routes.RouteJpaController.rou;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author adriano
 */
@ManagedBean
@ViewScoped
public class BeanRoute implements Serializable {

    /**
     * Creates a new instance of BeanUser
     */
    
    
    private Route routeSelect = new Route();
    private List<Route> routeList;
    
    public BeanRoute() {
    }

    public Route getRouteSelect() {
        return routeSelect;
    }

    public void setRouteSelect(Route routeSelect) {
        this.routeSelect = routeSelect;
    }

    public List<Route> getRouteList() {
        return rou.list();
    }

    public void setRouteList(List<Route> routeList) {
        this.routeList = routeList;
    }

   
    
    
    
   public void findById(long id){
        System.out.println(id);
        routeSelect = rou.findById(id);
        
    }
    
    public void edit(){
        int tamanhoD = this.routeSelect.getDescR().length();
        int tamanhoE = this.routeSelect.getExpression().length();
        
        if((tamanhoD!=0 && tamanhoE!=0)){
            
            rou.edit(routeSelect);
            FacesContext context = FacesContext.getCurrentInstance(); 
            context.addMessage(null, new FacesMessage("Sucesso!!",  "Rota Alterada!"));
            
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Campo(s) nulo(s)"));
        }
    }
    
    public void remove(){
        rou.remove(routeSelect);
        FacesContext context = FacesContext.getCurrentInstance(); 
        context.addMessage(null, new FacesMessage("Sucesso",  "Rota excluida com sucesso!"));
    }
    
   
    
}
    
