/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.apbx.routes;


import static br.com.usj.apbx.routes.RouteJpaController.rou;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author adriano
 */
@ManagedBean
@RequestScoped
public class BeanRouteAdd implements Serializable {

    /**
     * Creates a new instance of BeanUser
     */
    
    
    private Route routePersist = new Route();
    private short one = 1;
    private short zerado = 0;

    public short getOne() {
        return one;
    }

    public void setOne(short one) {
        this.one = one;
    }

    public short getZerado() {
        return zerado;
    }

    public void setZerado(short zerado) {
        this.zerado = zerado;
    }
    
    

    public Route getRoutePersist() {
        return routePersist;
    }

    public void setRoutePersist(Route routePersist) {
        this.routePersist = routePersist;
    }

    public void add(){
        
        System.out.println(routePersist.getDescR() + "==================");
        System.out.println(routePersist.getExpression());
        System.out.println(routePersist.getInbound());
        
        int tamanhoD = this.routePersist.getDescR().length();
        int tamanhoE = this.routePersist.getExpression().length();
        
        if((tamanhoD!=0 && tamanhoE!=0)){
           
            rou.add(routePersist);
            FacesContext context = FacesContext.getCurrentInstance(); 
            context.addMessage(null, new FacesMessage("Sucesso!!",  "Rota criada!"));
            
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Campo(s) nulo(s)!!"));
        } 
    }
    
}
    
