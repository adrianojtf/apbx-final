/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.apbx.routes;


import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;


/**
 *
 * @author adriano
 */
public class RouteJpaController implements Serializable {

    public static RouteJpaController rou = new RouteJpaController();
    
    EntityManager em = Persistence.
            createEntityManagerFactory("apbxPU").
            createEntityManager();

    public List<Route> list(){
        String query = "SELECT r FROM Route r";
        
        return em.createQuery(query).getResultList();
    }
    
    public void add(Route r){
        em.getTransaction().begin();
        em.persist(r);
        em.getTransaction().commit();
    }

    public void edit(Route r){
        long id = r.getId();
        em.getTransaction().begin();
        Route rot= em.find(Route.class, id);
        em.merge(r);
        rot.setDescR(r.getDescR());
        rot.setExpression(r.getExpression());
        rot.setInbound(r.getInbound());
        em.getTransaction().commit();
        
    }
    
    public void remove(Route r){
        em.getTransaction().begin();
        em.remove(r);
        em.getTransaction().commit();
    }
    
    public Route findById(long id){
      
        return em.find(Route.class, id);
    }
    
    
    
    
}
