/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.apbx.calls;



import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.Persistence;


/**
 *
 * @author adriano
 */
public class CallsJpaController implements Serializable {

    public static CallsJpaController call = new CallsJpaController();
    
    EntityManager em = Persistence.
            createEntityManagerFactory("apbxPU").
            createEntityManager();

    public List<Calls> list(){
        String query = "SELECT c FROM Calls c";
        
        return em.createQuery(query).getResultList();
    }
    
    
    public Calls findById(long id){
      
        return em.find(Calls.class, id);
    }
    
    public List<Calls> findByCallerid(String caller){
        
        String query = "SELECT e FROM Employee e WHERE e.dept = :deptName";
      
        return em.createQuery(query).setParameter("caller", caller).getResultList();    
    }
    
    
    
}
