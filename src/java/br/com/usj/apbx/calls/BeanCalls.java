/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.apbx.calls;



import static br.com.usj.apbx.calls.CallsJpaController.call;
import java.io.Serializable;

import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;


/**
 *
 * @author adriano
 */
@ManagedBean
@ViewScoped
public class BeanCalls implements Serializable {

    /**
     * Creates a new instance of BeanUser
     */
    
    
    private Calls callSelect = new Calls();
    private List<Calls> callsList;
    
    
    
    public BeanCalls() {
    }

    
    public Calls getCallSelect() {
        return callSelect;
    }

    public void setCallSelect(Calls callSelect) {
        this.callSelect = callSelect;
    }

    public List<Calls> getCallsList() {
        return call.list();
    }

    public void setCallsList(List<Calls> callsList) {
        this.callsList = callsList;
    }

   public void findById(long id){
        System.out.println(id);
        callSelect = call.findById(id);
        
    }
   
   public List<Calls> findByCallerId(){
       return call.findByCallerid(callSelect.getCallerid());
   }
    
    
}
    
