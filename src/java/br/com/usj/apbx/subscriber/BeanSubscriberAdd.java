/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.apbx.subscriber;

import static br.com.usj.apbx.subscriber.SubscriberJpaController.subs;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author adriano
 */
@ManagedBean
@RequestScoped
public class BeanSubscriberAdd implements Serializable {

    /**
     * Creates a new instance of BeanSubscriber
     */
    
    private Subscriber subscriber = new Subscriber();
   
    

    public BeanSubscriberAdd() {
        
    }

    
    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }

    
    public void add(){
        int tamanhoA = this.subscriber.getAccountcode().length();
        int tamanhoP = this.subscriber.getPassword().length();
        if(tamanhoA!=0 || tamanhoP!=0){
            subs.addSubscriber(subscriber);
            FacesContext context = FacesContext.getCurrentInstance(); 
            context.addMessage(null, new FacesMessage("Sucesso!!",  "Ramal criado!"));
            context.addMessage(null, new FacesMessage("Cuidado!!!", "Certifique-se de que escolheu uma senha segura!"));
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Ramal ou senha nulo(s)!!"));
        }
        
    }
    
      
        
}
