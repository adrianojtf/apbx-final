/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.apbx.subscriber;

import static br.com.usj.apbx.subscriber.SubscriberJpaController.subs;
import java.io.Serializable;
import java.util.List;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

/**
 *
 * @author adriano
 */
@ManagedBean
@ViewScoped
public class BeanSubscriber implements Serializable {

    /**
     * Creates a new instance of BeanSubscriber
     */
    
    
    private List<Subscriber> subList;
    private Subscriber subscriberSele = new Subscriber();
    private Subscriber subscriber = new Subscriber();
   
    

    public BeanSubscriber() {
        
    }

    
    public Subscriber getSubscriber() {
        return subscriber;
    }

    public void setSubscriber(Subscriber subscriber) {
        this.subscriber = subscriber;
    }
    
    

    
    public List<Subscriber> getSubList() {
        return subs.listSubscriber();
    }
    
    /**@PostConstruct
	public void init() {
		subscriber = new Subscriber();
	}*/

    public void setSubList(List<Subscriber> subList) {
        this.subList = subList;
    }

    public Subscriber getSubscriberSele() {
        return this.subscriberSele;
    }

    public void setSubscriberSele(Subscriber subscriber) {
        this.subscriberSele = subscriber;
    }
    
    public void findById(long id){
        System.out.println(id);
        subscriberSele = subs.findById(id);
        
    }
    
    public void edit(){
        int tamanhoA = this.subscriberSele.getAccountcode().length();
        int tamanhoP = this.subscriberSele.getPassword().length();
        if(tamanhoA!=0 && tamanhoP!=0){
            subs.edit(subscriberSele);
            FacesContext context = FacesContext.getCurrentInstance(); 
            context.addMessage(null, new FacesMessage("Sucesso!!",  "Ramal Alterado!"));
            context.addMessage(null, new FacesMessage("Cuidado!!!", "Certifique-se de que escolheu uma senha segura!"));
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Ramal ou senha nulo(s)!!"));
        }
    }
    
    public void remove(){
        subs.remove(subscriberSele);
        FacesContext context = FacesContext.getCurrentInstance(); 
        context.addMessage(null, new FacesMessage("Sucesso",  "Ramal excluido com sucesso!"));
    }
    
    public void add(){
        int tamanhoA = this.subscriber.getAccountcode().length();
        int tamanhoP = this.subscriber.getPassword().length();
        if(tamanhoA!=0 || tamanhoP!=0){
            subs.addSubscriber(subscriber);
            FacesContext context = FacesContext.getCurrentInstance(); 
            context.addMessage(null, new FacesMessage("Sucesso!!",  "Ramal criado!"));
            context.addMessage(null, new FacesMessage("Cuidado!!!", "Certifique-se de que escolheu uma senha segura!"));
        }else{
            FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Ramal ou senha nulo(s)!!"));
        }
        
    }
    
      
        
}
