/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.usj.apbx.subscriber;

import br.com.usj.apbx.subscriber.exceptions.NonexistentEntityException;
import br.com.usj.apbx.subscriber.exceptions.RollbackFailureException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.transaction.UserTransaction;
import java.lang.Class;

/**
 *
 * @author adriano
 */
public class SubscriberJpaController implements Serializable {

    
    
    public static SubscriberJpaController subs = new SubscriberJpaController();
    
    EntityManager em = Persistence.
            createEntityManagerFactory("apbxPU").
            createEntityManager();

    public List<Subscriber> listSubscriber(){
        String query = "SELECT s FROM Subscriber s";
        
        return em.createQuery(query).getResultList();
    }
    
    public void addSubscriber(Subscriber s){
        em.getTransaction().begin();
        em.persist(s);
        em.getTransaction().commit();
    }

    public void edit(Subscriber s){
        long id = s.getId();
        em.getTransaction().begin();
        Subscriber sub= em.find(Subscriber.class, id);
        em.merge(s);
        sub.setAccountcode(s.getAccountcode());
        sub.setPassword(s.getPassword());
        sub.setName(s.getName());
        sub.setLastname(s.getLastname());
        sub.setCallerid(s.getCallerid());
        em.getTransaction().commit();
        
    }
    
    public void remove(Subscriber s){
        em.getTransaction().begin();
        em.remove(s);
        em.getTransaction().commit();
    }
    
    public Subscriber findById(long id){
      
        return em.find(Subscriber.class, id);
    }
    
    
    
}
