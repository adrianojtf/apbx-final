APBX

Projeto piloto de uma interface para o freeswitch. Feito como projeto final da
Matéria Linguagem de programação II do curso de Analise e desenvolvimento de 
sistemas da Universidade de São José - USJ.

Detalhes do projeto:

Interface - Java - JSF - Primefaces
Banco de dados - MYSQL
Servidor SIP - FREESWITCH

Controle de projeto:

- PILOTO entrega final - Finalizado
- Controle de seção e acesso - Em andamento
- Melhorias de segurança(criptografia de senha) - Pendente
- Revisão e adequação das classes Subscriber, Route, Calls - Pendente
- Criação de novas classes conforme entidades necessárias no freeswitch - Pendente
- Integração Freeswitch - Pendente