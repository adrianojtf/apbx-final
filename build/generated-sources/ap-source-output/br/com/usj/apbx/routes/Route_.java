package br.com.usj.apbx.routes;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Route.class)
public abstract class Route_ {

	public static volatile SingularAttribute<Route, String> descR;
	public static volatile SingularAttribute<Route, String> expression;
	public static volatile SingularAttribute<Route, Boolean> inbound;
	public static volatile SingularAttribute<Route, Long> id;

}

