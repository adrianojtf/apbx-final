package br.com.usj.apbx.calls;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Calls.class)
public abstract class Calls_ {

	public static volatile SingularAttribute<Calls, Integer> duration;
	public static volatile SingularAttribute<Calls, Boolean> internal;
	public static volatile SingularAttribute<Calls, String> calleeid;
	public static volatile SingularAttribute<Calls, String> sip_call_id;
	public static volatile SingularAttribute<Calls, Long> id;
	public static volatile SingularAttribute<Calls, String> callerid;
	public static volatile SingularAttribute<Calls, String> direction;

}

