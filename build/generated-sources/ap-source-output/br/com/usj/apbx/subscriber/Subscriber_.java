package br.com.usj.apbx.subscriber;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Subscriber.class)
public abstract class Subscriber_ {

	public static volatile SingularAttribute<Subscriber, String> password;
	public static volatile SingularAttribute<Subscriber, String> name;
	public static volatile SingularAttribute<Subscriber, Long> id;
	public static volatile SingularAttribute<Subscriber, String> callerid;
	public static volatile SingularAttribute<Subscriber, String> accountcode;
	public static volatile SingularAttribute<Subscriber, String> lastname;

}

